var app = angular.module('hc', ['ngRoute', 'ngCookies', 'mm.foundation', 'smoothScroll'] );
app.run(function($rootScope, $location){
  $rootScope.thisYear = new Date().getFullYear();
  window.rs = $rootScope;
  $rootScope.$on('$routeChangeSuccess', function(){
    $rootScope.location = $location;
     window.scrollTo(0,0);
  });
});
