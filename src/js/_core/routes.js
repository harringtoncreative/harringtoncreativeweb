app.config(function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/', {
    templateUrl: '/tpl/public/home.html',
    controller: 'homeCtrl',
    title:'Home'
  })
   .when('/services/development', {
    templateUrl: '/tpl/public/services/development.html',
    controller: 'developmentCtrl',
    title:'Development'
  })
   .when('/services/design', {
    templateUrl: '/tpl/public/services/design.html',
    controller: 'designCtrl',
    title:'Design'
  })
   .when('/ourwork', {
    templateUrl: '/tpl/public/ourwork.html',
    controller: 'ourworkCtrl',
    title:'Our Work'
  })
   .when('/ourwork/:slug', {
    templateUrl: '/tpl/public/ourwork-workpage.html',
    controller: 'ourworkWorkPageCtrl',
    title:'Our Work'
  })
   .when('/contact', {
    templateUrl: '/tpl/public/contact.html',
    controller: 'contactCtrl',
    title:'Contact Us'
  });

  $routeProvider.otherwise({redirectTo: '/'});

  // configure html5 to get links working on jsfiddle
  $locationProvider.html5Mode(true).hashPrefix('!');
});
