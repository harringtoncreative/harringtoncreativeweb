app.controller('ourworkWorkPageCtrl', function($scope, $sce, $http, $routeParams) {
  $http.get('/data/work.json').success(function(d){
    index = d.map(function(obj, index) {if(obj.slug == $routeParams.slug) { return index; } }).filter(isFinite)[0]+1;
    console.log(index);
    $scope.next= d[index];
    $scope.work = d.filter(function(o){ return o.slug === $routeParams.slug;})[0];
  }); 
});
