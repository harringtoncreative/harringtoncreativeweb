app.controller('menuBarCtrl', function($scope, $location){
  
  $scope.currentPage = function(link){
    var path = $location.path() === '/' ? 'home' : $location.path();
    return path.indexOf(link) > -1?'active':'';
  };

});