app.controller('contactCtrl', function($scope, $sce, $http, $modal) {
  

  $scope.sendPush = function(){

    if ($scope.contactForm.$valid) {
      console.log($scope.message);
      var message = {
        "type": "note",
        "title": "Harrington Creative Message",
        "body": JSON.stringify($scope.message, null, 2)
      };

      $http({
        method: 'POST', 
        url: 'https://api.pushbullet.com/v2/pushes', 
        headers: {'Authorization': 'Bearer v1kYhRmWu6GJilvEgaP0y66RKYl5AuiBkNujwZa976gvs'},
        data: message
      }).success(function(d){
        $scope.message = {};

        $modal.open({ templateUrl: 'myModalContent.html', windowClass:'small' });

      });
    }
  };

});

