app.controller('homeCtrl', function($scope, $sce) {
  $scope.vision = $sce.trustAsHtml("Built on passion we inspire to not only create <b>beautiful</b> websites but, to <b>challenge</b> the status quo and <b>change</b> the way you do business.");
});
