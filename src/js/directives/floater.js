app.directive('floater', function($timeout) {
  return {
    restrict: 'A',
    link:function($scope, element, attrs){
      var doFloat = false;

      $(window).on('resize', function(){
        if ($(window).outerWidth() > 1020) {doFloat = true; }else{doFloat=false;}
      });

    
      $timeout(function(){
        
        if ($(window).outerWidth() > 1020) {
          doFloat = true;
        }
        var bottomMargin = 150;
        var topMargin = 70;
        var MFS = $(element);
        var ogScrollTop     = $('body').offset().top,
            ogElementOffset = MFS.offset().top,
            ogDistance = (ogElementOffset - ogScrollTop);

        MFSheight = MFS.outerHeight();
        documentHeight = $(document).outerHeight();
        $('body').change(function(){
          MFSheight = MFS.outerHeight();
        });

        $(window).on('scroll', function(){
          if (doFloat) {
            var offsetTop     = $('body').offset().top,
            elementOffset = MFS.offset().top,
            distance      = (elementOffset - offsetTop),
            scrollTop  = $(window).scrollTop();

            if(scrollTop > ogDistance - topMargin ){
              if(scrollTop < documentHeight - MFSheight - $('footer').outerHeight()){
                MFS.css({"position":"relative","top": scrollTop- ogDistance +topMargin});
              }else{
                MFS.css({"position":"relative","top":  documentHeight - MFSheight - $('.footer').outerHeight() - ogDistance - bottomMargin});
              }
            }else{
              MFS.css({"position":"relative","top": 0 });
            }
          }else{
            MFS.css({"position":"relative","top": 0 });
          }

        });
      },200);




    }
  };
});